# movieapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Début du projet : 18/08/2019
### Version Béta (No PWA) : 18/12/2019

```
json-server --watch movies.json
json-server --watch actors.json --port 4300

Movies stored at : 
https://www.npoint.io/docs/23d900f83d2e4752aeed