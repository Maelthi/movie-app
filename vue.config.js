const GoogleFontsPlugin = require("google-fonts-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;


module.exports = {
    css: {
      loaderOptions: {
        sass: {
          data: `@import "@/assets/_global.scss";`
        }
      }
    },
    configureWebpack: {
      devtool: 'source-map'
    }, 
    chainWebpack: config => {
      plugins: [
          new GoogleFontsPlugin({
              fonts: [
                  { family: "Source Sans Pro" }
              ]
          }), [new BundleAnalyzerPlugin()]
      ]
   }
  };