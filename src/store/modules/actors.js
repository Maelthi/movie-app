import ActorsService from '@/services/ActorsService.js'

export const namespaced = true

export const state = {
    actors: [],
    actor: {}, 
    loaded: false, 
    checkedActors: []
}

export const getters = {
    actorsLength: state => {
        return state.actors.length
    }, 
    getActorByName: state => acteur => {
        return state.actors.find(actor => actor.patronyme === acteur)   
    }, 
    checkActors: state => el => {
        return state.actors.filter(actor => el.includes(actor.patronyme)) 
    },
    getLoaded: state => {
        return state.loaded 
    }, 
    getActors: state => {
        return state.actors
    }
}

export const mutations = {
    SET_ACTORS(state, actors) {
        state.actors = actors
    }, 
    SET_ACTOR(state, actor){
        state.actor = actor
    },
    SET_LOADER(state, value){
        state.loaded = value
    },
    SET_CHECKEDACTORS(state, checkedActors) {
        state.checkedActors = checkedActors
    }, 
    SORT_ACTORS(state) {
        // Tri par ordre alphabétique
        state.actors.sort(function(a, b){
            var nameA=a.patronyme.toLowerCase(), nameB=b.patronyme.toLowerCase();
            if (nameA < nameB)
             return -1;
            if (nameA > nameB)
             return 1;
            return 0;
           });     
    }
}

export const actions = {
    fetchActors({commit}) {
        ActorsService.getActors()
            .then(response => {
                commit('SET_ACTORS', response.data)
                commit('SORT_ACTORS')
            })
            .catch(error => {
                console.log(`${error}`)
            })
    }, 
    fetchActor({commit}, id) {
        return ActorsService.getActors(id) 
            .then(response => {
                commit('SET_ACTOR', response.data[id])
            })
    }, 
    async checkActors({getters, commit}, acteurs) {
        await ActorsService.getActors()
            .then(response => {                
                commit('SET_ACTORS', response.data)
                let checkActors = getters.checkActors(acteurs)
                commit('SET_CHECKEDACTORS', checkActors)
            })
            .catch(error => {
                console.log(`${error}`);
            })
    }, 
    fetchActorByName({getters, commit}, acteur) {
        let actor = getters.getActorByName(acteur)
        console.log(actor);

        
        
        if (actor) {
            commit('SET_ACTOR', actor)
        } else {
            commit('SET_ACTOR', {})
        }
    }
}