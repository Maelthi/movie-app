import MoviesService from '@/services/MoviesService.js'

export const namespaced = true

export const state = {
    movies: [], 
    moviesCopy: [],
    randomMovie:{},
    movie: {}, 
    loaded: false, 
    sorted: '', 
    checkedMovies: [], 
    error: ''
}

export const getters = {
    moviesLength: state => {
        return state.movies.length
    }, 
    getMovieByName: (state) => titre => {
        return state.movies.find(movie => movie.titre === titre)
    },
    checkMovies: (state) => el => {
        return state.movies.filter(movie => el.includes(movie.titre))
    },
    getLoaded: state => {
        return state.movies.loaded
    }, 
    getMovies: state => {
        return state.movies
    }, 
    getMoviesByGenre: (state) => genre => {
        return state.movies.filter(movie => movie.genre === genre)
    }
}

export const mutations = {
    SET_MOVIES(state, movies) {
        state.movies = movies
    }, 
    SET_MOVIESCOPY(state, movies) {
        state.moviesCopy = movies;
    }, 
    SET_MOVIESCOPY2(state) {
        state.movies = state.moviesCopy;
    }, 
    SET_DAILYMOVIE(state, movie) {
        state.randomMovie = movie
    }, 
    SET_MOVIE(state, movie) {
        state.movie = movie
    }, 
    SET_CHECKEDMOVIES(state, checkedMovies) {
        state.checkedMovies = checkedMovies
    }, 
    SET_LOADER(state, value) {
        state.loaded = value
    },
    SORT_MOVIES_NAME(state) {
        // Tri par ordre alphabétique
        state.movies.sort(function(a, b){
            var nameA=a.titre.toLowerCase(), nameB=b.titre.toLowerCase();
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });    
    }, 
    SORT_MOVIES_DATE(state) {
        // Tri par date de sortie
           state.movies.sort(function(a, b) {
                return a.dateSortie - b.dateSortie
           })
    }, 
    SORT_MOVIES_RANDOM(state) {
        // Tri aléatoire
        state.movies.sort(function(a, b) {
            const rand1 = Math.floor(Math.random() * (1 - 200 + 1) + 1)
            const rand2 = Math.floor(Math.random() * (1 - 200 + 1) + 1)
            return rand1 - rand2 
        })        
    }, 
    FILTER_GENRE(state, movies) {
        state.movies = movies
    }, 
    SET_ERROR(state) {
        state.error = state
    }
}

export const actions = {
    fetchMovies({commit}) {
        MoviesService.getMovies()
            .then(response => {                
                commit('SET_MOVIES', response.data)
                commit('SET_MOVIESCOPY', response.data)
            })
            .catch(error => {
                commit('SET_ERROR', error)
            })
    }, 
    fetchRandomMovie({commit, getters}) {
        MoviesService.getMovies()
            .then(response => {
                let rand = Math.floor((Math.random() * getters.moviesLength) + 1);
                commit('SET_DAILYMOVIE', response.data[rand])
            })
            .catch(error => {
                commit('SET_ERROR', error)
            })
    }, 
    fetchMovie({commit}, id) {
        return MoviesService.getMovies()
            .then(response => {
                commit('SET_MOVIE', response.data[id])
            })
    }, 
    fetchMovieByName({getters, commit}, apparition) {
        let movie = getters.getMovieByName(apparition)
        if (movie) {
            commit('SET_MOVIE', movie)
        } 
    }, 
    async checkMovies({getters, commit}, apparitions) {
        await MoviesService.getMovies()
            .then(response => {                
                commit('SET_MOVIES', response.data)
                let checkedMovies = getters.checkMovies(apparitions)
                commit('SET_CHECKEDMOVIES', checkedMovies)
            })
            .catch(error => {
                console.log('error', error);
            })
    }, 
    fetchMoviesByGenre({commit, getters}, genre) {
        let moviesGenre = getters.getMoviesByGenre(genre) 
        if (moviesGenre) {
            commit('FILTER_GENRE', moviesGenre)
        }
    }
}