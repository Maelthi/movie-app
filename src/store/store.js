import Vue from "vue";
import Vuex from "vuex";
import * as movies from "@/store/modules/movies.js"
import * as actors from "@/store/modules/actors.js"

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    movies, 
    actors
  }
});
