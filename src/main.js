import Vue from "vue";
import App from "./App.vue";
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import router from "./router";
import store from "./store/store";
import "./registerServiceWorker";
import "@/assets/_global.scss";
import MaxLength from './filters/maxLength.js'
import VueAnime from 'vue-animejs';
import VueRellax from 'vue-rellax'
import Vuelidate from 'vuelidate'



Vue.filter('maxLength', MaxLength)
Vue.use(VueAnime)
Vue.use(VueRellax)
Vue.use(Vuelidate)
Vue.config.productionTip = false;

export const eventBus = new Vue ({

  methods: {
    sortCliqued(isDeployed) {
      this.$emit('sortCliqued', isDeployed);
    }, 
    filterCliqued(isDeployed) {
      this.$emit('filterCliqued', isDeployed);
    },
    optionSelected() {
      this.$emit('optionSelected', false)
    },
    genreSelected(genre) {
      this.$emit('genreSelected', genre)
    },
    sortByName() {
      this.$emit('sortByName', true)
    }, 
    sortByDate() {
      this.$emit('sortByDate', true)
    }, 
    sortRandomly() {
      this.$emit('sortRandomly', true)
    }, 
    resetMovies() {
      this.$emit('resetMovies')
    }
  }
});

const requireComponent = require.context(
  './components',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )
  Vue.component(componentName, componentConfig.default || componentConfig)
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
