import axios from 'axios'
import store from '../store/store'

const apiClient = axios.create({
    withCredentials: false,
    baseURL: 'https://api.npoint.io/23d900f83d2e4752aeed',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})

apiClient.interceptors.request.use(config => {
    return config
})

apiClient.interceptors.response.use(config => {
    store.commit('movies/SET_LOADER', true)
    return config
})

export default {
    getMovies() {
        return apiClient.get()
    }, 
    addMovies() {
        return apiClient.post()
    }
}