import axios from 'axios'
import store from '../store/store'

const apiClient = axios.create({
    withCredentials: false,
    baseURL: 'https://api.npoint.io/8ef4942d1d7c43dbca5b',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})

apiClient.interceptors.request.use(config => {
    return config
})

apiClient.interceptors.response.use(config => {
    store.commit('actors/SET_LOADER', true)
    return config
})

export default {
    getActors() {
        return apiClient.get()
    }
}