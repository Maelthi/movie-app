export default value => {
    if (value.length > 200) {
        return `${value.substring(0,200)}...`
    } else {
        return value;
    }
}
