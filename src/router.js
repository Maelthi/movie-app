import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import ActorsList from "./views/ActorsList.vue";
import MoviesList from "./views/MoviesList.vue";
import MovieDetails from "./views/MovieDetails.vue";
import ActorDetails from "./views/ActorDetails.vue";
import NotFound from "./views/NotFound.vue";
import BackOffice from "./views/BackOffice.vue";

Vue.use(Router);

  

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    if (from.name == "movies" || from.name == "actors" || from.name == "movieDetails" || from.name == "actorDetails" || from.name == "home") {
      return {x:0, y:0}
    }
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/actors",
      name: "actors",
      component: ActorsList
    },
    {
      path: "/movies",
      name: "movies",
      component: MoviesList
    }, 
    {
      path: "/movies/:id", 
      name: "movieDetails",
      component: MovieDetails
    }, 
    {
      path: "/actors/:id", 
      name: "actorDetails", 
      component: ActorDetails
    }, {
      path: "/back-office", 
      name: "backOffice", 
      component: BackOffice
    }, {
      path: "*", 
      name: "404", 
      component: NotFound
    }
  ]
});

export default router
